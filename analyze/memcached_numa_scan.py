import os
import util

def parse_memcached_output(s):
    lines = s.split('\n')
    return int(float(lines[6].split(' ')[3]))

def parse_memcached_stat(s, qps):
    lines = s.split('\n')
    inst = 0
    cycle = 0
    for line in lines:
        if line.startswith('#'):
            continue
        split = line.strip().split()
        if len(split) <= 1:
            continue
        if int(float(split[0])) != 25:
            continue
        if split[2] == 'instructions:u' or split[2] == 'instructions:k':
            inst += int(split[1].replace(',', ''))
        if split[2] == 'cycles:u' or split[2] == 'cycles:k':
            cycle += int(split[1].replace(',', ''))
    return (float(inst) / 5 / qps, float(inst) / cycle)

def extract_numa_scan_exp(exp):
    ret = ([], [], [], [], [], []) # QPS_mean, QPS_stddev, IPQ_mean, IPQ_stddev, IPC_mean, IPC_stddev

    for t in [2, 4, 8, 16]:
        f = 't' + str(t)
        (qps_mean, qps_stddev) = \
            util.extract_exp_avg_stddev(exp, f, parse_memcached_output)

        stat = 'stat_' + f
        ((ipq_mean, ipc_mean), (ipq_stddev, ipc_stddev)) = \
            util.extract_exp_avg_stddev(exp, stat, parse_memcached_stat, qps_mean)

        ret[0].append(qps_mean)
        ret[1].append(qps_stddev)
        ret[2].append(ipq_mean)
        ret[3].append(ipq_stddev)
        ret[4].append(ipc_mean)
        ret[5].append(ipc_stddev)

    return ret

def collect_data(experiments):
    ret = dict()

    for exp in experiments:
        ret[exp] = extract_numa_scan_exp(exp)

    return ret
