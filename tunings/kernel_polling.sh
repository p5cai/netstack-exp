if [ ! "$(uname -r)" == "5.15.79-peter" ]; then
    echo "kernel polling only supported on patched kernel"
    exit 1
fi

sysctl net.core.busy_poll=16384
# Set rx ring size to maximum
ethtool -G $NET_IF rx 8192
# NAPI locality is needed for multi-threaded experiments
NAPI_LOCALITY=true
export MEMCACHED_TARGET_QPS_PER_THREAD=130000
