markers = {
  "Vanilla": ".",
  "Kernel Polling": "+",
  "IRQ Suppression": "x",
  "IRQ Packing": "*",
  "F-Stack": "d",
}

colors = {
  "Vanilla": "tab:blue",
  "Kernel Polling": "tab:orange",
  "IRQ Suppression": "tab:purple",
  "IRQ Packing": "tab:red",
  "F-Stack": "tab:cyan",
}

patterns = {
  "Vanilla": "//",
  "Kernel Polling": "..",
  "F-Stack": "--",
}
