import matplotlib.pyplot as plt
from memcached_conn_scan import extract_conn_qps_exp
import os

def plot():
    data = list()
    data.append(extract_conn_qps_exp("memcached_conn_scan.5.4.0-136-generic.caladan.t2"))
    data.append(extract_conn_qps_exp("memcached_conn_scan.5.4.0-136-generic.caladan.t4"))
    data.append(extract_conn_qps_exp("memcached_conn_scan.5.4.0-136-generic.caladan.t6"))
    data.append(extract_conn_qps_exp("memcached_conn_scan.5.4.0-136-generic.caladan.t8"))

    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    plt.title("Memcached Caladan QPS / \"IPR\" vs Threads, 160 connections", y = 1.05)

    ax2.bar([2, 4, 6, 8], [d[5][4] for d in data], yerr = [d[6][4] for d in data], capsize = 4, fill = False, hatch = '/')
    ax1.errorbar([2, 4, 6, 8], [d[1][4] for d in data], yerr = [d[2][4] for d in data], capsize = 4)

    ax1.ticklabel_format(axis = 'both', style = 'sci', scilimits = (0, 4))
    ax2.ticklabel_format(axis = 'both', style = 'sci', scilimits = (0, 3))
    ax1.set_xlabel("Threads")
    ax1.set_ylabel("QPS")
    ax2.set_ylabel("Instruction per Request (\"IPR\")")

    if os.getenv('SAVE_FIGURE') != 'true':
        plt.rcParams.update({'font.size': 16})
        plt.show()
    else:
        plt.savefig('../data/figs/memcached_caladan_qps_ipr_vs_threads.png', dpi = 192)

plot()
