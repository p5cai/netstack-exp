#!/bin/bash

PWD_ORIG="$PWD"

PERF=perf

if [ -e "../configs/$(uname -r).sh" ]; then
    echo "Loading kernel-specific config for $(uname -r)"
    source ../configs/$(uname -r).sh
fi

EXP_NAME="$1"

if [ -z "$EXP_NAME" ] || [ ! -d "../data/$EXP_NAME" ]; then
    echo "Invalid experiment $EXP_NAME"
    exit 1
fi

if [[ "$EXP_NAME" =~ ^.*caladan.*$ ]]; then
    IS_CALADAN=true
fi

if [[ "$EXP_NAME" =~ ^.*fstack.*$ ]]; then
    IS_FSTACK=true
fi

SAMPLES_KERNEL=0
SAMPLES_LIBRARY=0
SAMPLES_APP=0

pushd "../data/$EXP_NAME"

# Use perf report headers to determine the interval to capture
PERF_HEADERS="$($PERF report --header-only)"
EXP_START="$(echo "$PERF_HEADERS" | grep "# time of first sample :" | rev | cut -d' ' -f1 | rev | cut -d'.' -f1)"
EXP_END="$(echo "$PERF_HEADERS" | grep "# time of last sample :" | rev | cut -d' ' -f1 | rev | cut -d'.' -f1)"

printf 'Experiment: %d - %d\n' $EXP_START $EXP_END | tee breakdown.txt

MEASURE_END=$((EXP_END - (EXP_END - EXP_START) % 5 - 5))
MEASURE_START=$((MEASURE_END - 10))

printf 'Measurement Interval: %d - %d\n' $MEASURE_START $MEASURE_END | tee -a breakdown.txt

while read line; do
    [[ -z "$line" ]] && continue
    [[ "$line" =~ ^#.*$ ]] && continue
    samples="$(echo "$line" | awk '{ print $2; }')"
    obj="$(echo "$line" | awk '{ print $4; }')"
    symbol="$(echo "$line" | awk '{ print $6; }')"
    if [ "$obj" == "memcached" ] || [ "$obj" == "nginx" ]; then
        if [[ "$IS_CALADAN" == true ]] && grep "$symbol" "$PWD_ORIG/caladan.syms" > /dev/null; then
            SAMPLES_LIBRARY=$((SAMPLES_LIBRARY + samples))
        elif [[ "$IS_FSTACK" == true ]] && grep "$symbol" "$PWD_ORIG/fstack.syms" > /dev/null; then
            SAMPLES_LIBRARY=$((SAMPLES_LIBRARY + samples))
        else
            SAMPLES_APP=$((SAMPLES_APP + samples))
        fi
    elif [[ "$obj" =~ ^\[.*\]$ ]]; then
        SAMPLES_KERNEL=$((SAMPLES_KERNEL + samples))
    else
        SAMPLES_LIBRARY=$((SAMPLES_LIBRARY + samples))
    fi
done <<< "$($PERF report --stdio --no-children -n --time $MEASURE_START,$MEASURE_END)"

SAMPLES=$((SAMPLES_KERNEL + SAMPLES_LIBRARY + SAMPLES_APP))
printf 'Kernel: %d (%d%%)\n' $SAMPLES_KERNEL $((SAMPLES_KERNEL * 100 / SAMPLES)) | tee -a breakdown.txt
printf 'Library: %d (%d%%)\n' $SAMPLES_LIBRARY $((SAMPLES_LIBRARY * 100 / SAMPLES)) | tee -a breakdown.txt
printf 'App: %d (%d%%)\n' $SAMPLES_APP $((SAMPLES_APP * 100 / SAMPLES)) | tee -a breakdown.txt

popd
