source "$EXP_ROOT/experiments/extra.shared.sh"

CONNS=160 # per agent thread

NO_RUNS=0

REINIT_RUNS=4

load_config() {
    source "$EXP_ROOT/configs/memcached.sh"
    source "$EXP_ROOT/configs/mutilate.sh"
}

cleanup_mutilate() {
    echo "Killing all mutilate processes on clients"
    sudo -u $SSH_USER pdsh -R ssh -w $MEMCLIENTS pkill mutilate || true
}

cleanup_memcached() {
    echo "Killing memcached process"
    kill -INT $MEMCACHED_PID || true
    sleep 2
    kill $MEMCACHED_PID || true
    if [ "$PERF_STAT_ONLY_MEMCACHED" == "true" ]; then
        PERF_EXTRA_ARGS="$PERF_EXTRA_ARGS_ORIG"
    fi
}

start_memcached() {
    echo "Starting memcached"
    if [ "$NAPI_LOCALITY" == "true" ]; then
        MEMCACHED_NAPI_ARGS="-N $THREADS"
    fi
    echo "taskset -c $FIRST_CPU-$((FIRST_CPU + THREADS - 1 - THREAD_TAIL_OFFSET)) $MEMCACHED $MEMCACHED_ARGS $MEMCACHED_NAPI_ARGS -u root -t $((THREADS - THREAD_TAIL_OFFSET)) -b 16384 -c 32768 -m 10240 -o hashpower=24,no_lru_maintainer,no_lru_crawler" 
    taskset -c $FIRST_CPU-$((FIRST_CPU + THREADS - 1 - THREAD_TAIL_OFFSET)) $MEMCACHED $MEMCACHED_ARGS $MEMCACHED_NAPI_ARGS -u root -t $((THREADS - THREAD_TAIL_OFFSET)) -b 16384 -c 32768 -m 10240 -o hashpower=24,no_lru_maintainer,no_lru_crawler &
    MEMCACHED_PID=$!
    if [ "$PERF_STAT_ONLY_MEMCACHED" == "true" ]; then
        PERF_EXTRA_ARGS_ORIG="$PERF_EXTRA_ARGS"
        PERF_EXTRA_ARGS="-p $MEMCACHED_PID $PERF_EXTRA_ARGS"
        PERF_STAT_NO_CORES=true
    fi
}

start_mutilate() {
    echo "Starting mutilate"
    # Mutilate client agents always run on 8 cores
    sudo -u $SSH_USER pdsh -R ssh -w $MEMCLIENTS taskset -c 0-7 mutilate -A -T 8 2>/dev/null & sleep 1
}

warmup() {
    echo "Warming up Memcached..."
    sudo -u $SSH_USER $MUTILATE --loadonly
    sudo -u $SSH_USER $MUTILATE --noload -c100 $MUTCLIENTS -t10 -u 0
}

run_mutilate() {
    if [ "$NO_RUNS" -ge "$REINIT_RUNS" ]; then
        echo "Re-intializing Memcached server"
        memcached_shutdown
        memcached_startup
        NO_RUNS=0
    fi
    echo "Running mutilate with $CONNS connections per agent (core) and QPS = $QPS"
    perf_stat_start "$1"
    extra_data_start $MEMCTRL,$MEMCLIENTS
    sudo -u $SSH_USER $MUTILATE --noload -c$CONNS $MUTCLIENTS -t30 -u 0 -q $QPS | tee $DATA_OUT/$1$EXP_AUX_SUFFIX.txt
    extra_data_stop $MEMCTRL,$MEMCLIENTS "$1"
    perf_stat_stop
    NO_RUNS=$((NO_RUNS + 1))
}

memcached_pre_start() {
    load_config

    if [ "$FSTACK" == "true" ] && [ $THREADS -gt 1 ]; then
        echo "Cannot run F-Stack memcached in multi-threaded mode"
        exit 1
    fi

    if [ "$FSTACK" == "true" ]; then
        MEMCACHED="$MEMCACHED_FSTACK"
    elif [ "$CALADAN" == "true" ]; then
        MEMCACHED="$MEMCACHED_CALADAN"
        MEMCACHED_ARGS="$MEMCACHED_ARGS $CALADAN_CONFIG -p 11211"
        REINIT_RUNS=9999 # Caladan does not support restarting the application process properly
    fi

    EXPERIMENT_NAME_EXT="t$THREADS.c$CONNS"

    pkill memcached || true
    sleep 1
    cleanup_mutilate    
}

memcached_init() {
    NO_RUNS=0
    memcached_startup
}

memcached_startup() {
    start_memcached
    start_mutilate
    sleep 15
    warmup
}

memcached_shutdown() {
    cleanup_mutilate
    cleanup_memcached
}

memcached_deinit() {
    memcached_shutdown
}

