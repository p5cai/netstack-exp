# Networking parameters
sysctl -w net.core.somaxconn=1048576
sysctl -w net.ipv4.tcp_max_syn_backlog=262144
sysctl -w net.ipv4.tcp_tw_reuse=1
sysctl -w net.ipv4.ip_local_port_range="1024 65535"
sysctl -w vm.max_map_count=2097152

# Debugging options
echo 0 > /proc/sys/kernel/yama/ptrace_scope         # gdb attach
echo -1 > /proc/sys/kernel/perf_event_paranoid      # perf anything
echo 0 > /proc/sys/kernel/kptr_restrict             # perf tracing
echo 0 > /proc/sys/kernel/nmi_watchdog              # perf cache tracing

# CPU frequency
$X86_ENERGY_PERF_POLICY --turbo-enable 0
$CPUPOWER frequency-set -f 2.7GHz || true # This can throw out errors but it's generally harmless

# Default non-polling NIC settings
# ethtool may fail without fatal errors (e.g. when it made no changes)
ethtool -G $NET_IF rx 1024 || true
ethtool -C $NET_IF rx-usecs  16 rx-frames   44 adaptive-rx on || true
ethtool -C $NET_IF tx-usecs  16 tx-frames   16 adaptive-tx off || true
sysctl net.core.busy_poll=0
sysctl net.core.busy_read=0
sysctl net.core.dev_weight=64
sysctl net.core.netdev_budget=300
sysctl net.core.netdev_budget_usecs=8000

# Prevent the kernel from doing any of its NUMA magic
sysctl kernel.numa_balancing=0

export THREAD_TAIL_OFFSET=0
export MEMCACHED_TARGET_QPS_PER_THREAD=95000
