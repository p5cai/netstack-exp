if [ "$(uname -r)" == "5.15.79-peter" ]; then
    echo "Cannot use manual tuning hacks on custom kernel with true polling support"
    exit 1
fi

ethtool -G $NET_IF rx 8192
ethtool -C $NET_IF rx-usecs 65534 rx-frames 65534 adaptive-rx off # 65535 - default
ethtool -C $NET_IF tx-usecs  1024 tx-frames   256 adaptive-tx off # 65535 - default
sysctl net.core.busy_poll=65535
sysctl net.core.busy_read=65535
sysctl net.core.dev_weight=1024
sysctl net.core.netdev_budget=1024
sysctl net.core.netdev_budget_usecs=65535

source $EXP_ROOT/tunings/normalize_queues.sh

NAPI_LOCALITY=true
export MEMCACHED_TARGET_QPS_PER_THREAD=130000
