source "$EXP_ROOT/experiments/memcached.shared.sh"

pre_start() {
    memcached_pre_start
    EXPERIMENT_NAME_EXT="t$THREADS"
    if [ "$FSTACK" == "true" ]; then
        REINIT_RUNS=3
    fi
    PERF_STAT_DELTA=true
}

run_exp() {
    memcached_init

    QPS=0
    for CONNS in 10 20 40 60 80 100; do
        export CONNS
        run_mutilate c$CONNS
        sleep 10
    done    

    memcached_deinit
}

post_start() {
    memcached_deinit
}
