import matplotlib.pyplot as plt
from memcached_numa_scan import collect_data
from matplotlib.legend_handler import HandlerTuple
import os
from config import colors, patterns, markers

experiments = [
    'memcached_numa_scan.5.15.79-peter', # vanilla
    'memcached_numa_scan.5.15.79-peter.kernel_polling', # kernel_polling
]

labels = [
    'Vanilla',
    'Kernel Polling',
]

def plot():
    data = collect_data(experiments)
    plt.cla()

    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()

    plt.xticks([1.5, 4.5, 7.5, 10.5], ["1 + 1", "2 + 2", "4 + 4", "8 + 8"])
    l = len(experiments)
    for i in range(l):
        offset = (0.4 * i + 0.2) - 0.4 * l / 2
        exp = experiments[i]
        ax1.bar([1 + offset, 4 + offset, 7 + offset, 10 + offset], data[exp][2], 0.4, yerr = data[exp][3], capsize=4, label = labels[i], edgecolor = colors[labels[i]], hatch = patterns[labels[i]], fill = False)
        ax2.bar([2 + offset, 5 + offset, 8 + offset, 11 + offset], data[exp][4], 0.4, yerr = data[exp][5], capsize = 4, label = labels[i], edgecolor = colors[labels[i]], hatch = patterns[labels[i]] + '.', linestyle="--", fill = False)

    ax1.set_xlabel('Core Configuration')
    ax1.set_ylabel('IPQ')
    ax2.set_ylabel('IPC')
    ax1.set_ylim(0, 25000)
    ax2.set_ylim(0, 1)

    ax2.legend(loc = 'upper right')
    ax1.legend(loc = 'upper left')
    if os.getenv('SAVE_FIGURE') != 'true':
        plt.rcParams.update({'font.size': 16})
        plt.show()
    else:
        plt.savefig('../data/figs/memcached_numa_scan_ipc.png', dpi = 192)

plot()
