import os
import util

def parse_memcached_output(s):
    lines = s.split('\n')
    qps = int(float(lines[6].split(' ')[3]))
    latency_99th = int(float(lines[1].split(' ')[-1]))
    return (qps, latency_99th)

def extract_qps_latency_exp(exp):
    ret = ([], [], [])

    for f in sorted(os.listdir(os.path.join('../data/', exp)), key = lambda x: f"{x:>20}"):
        if not f[0].isdigit():
            continue

        if f.split('_')[-1].startswith('aux'):
            continue

        ((qps_mean, latency_99th_mean), (qps_stddev, latency_99th_stddev)) = \
            util.extract_exp_avg_stddev(exp, f.replace('.txt', ''), parse_memcached_output)

        ret[0].append(qps_mean)
        ret[1].append(latency_99th_mean)
        # We don't care about stddev in qps -- qps is the x axis
        ret[2].append(latency_99th_stddev)

    return ret

def extract_qps_latency(experiments):
    ret = dict()

    for exp in experiments:
        ret[exp] = extract_qps_latency_exp(exp)

    return ret
