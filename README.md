Kernel Patch
============

Note that this repo contains the kernel patch that was used for the experiments reported in the paper [Kernel vs. User-Level Networking: Don't Throw Out the Stack with the Interrupts](https://cs.uwaterloo.ca/~mkarsten/papers/sigmetrics2024.html).

However, a practical implementation of the interrupt mitigation scheme described in the paper has been submitted as an RFC for potential inclusion into the Linux kernel:

- [https://lore.kernel.org/netdev/20240812125717.413108-1-jdamato@fastly.com/](https://lore.kernel.org/netdev/20240812125717.413108-1-jdamato@fastly.com/)
