import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerTuple
from memcached_conn_scan import collect_data
from config import markers, colors, patterns
import os

experiments_t1 = [
    "memcached_conn_scan.5.15.79-peter.t1", # vanilla
    "memcached_conn_scan.5.15.79-peter.kernel_polling.t1", # kernel polling
    "memcached_conn_scan.5.4.0-136-generic.fstack.t1", # fstack
]

experiments_t8 = [
    "memcached_conn_scan.5.15.79-peter.t8", # vanilla
    "memcached_conn_scan.5.15.79-peter.kernel_polling.t8", # kernel polling
]

labels = [
    "Vanilla",
    "Kernel Polling",
    "F-Stack",
]

def plot(threads):
    if threads == 1:
        experiments = experiments_t1
    else:
        experiments = experiments_t8
    data = collect_data(experiments)
    plt.cla()

    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()

    for i in range(len(experiments)):
       exp = experiments[i]
       label = labels[i]
       ax1.errorbar(data[exp][0], [data[exp][1][j] / 1000 for j in range(len(data[exp][1]))], yerr = [data[exp][2][j] / 1000 for j in range(len(data[exp][2]))], capsize = 4, label = label, marker = markers[label], color = colors[label])

       offset = (3 * i + 1.5) - 3 * len(experiments) / 2
       ax2.bar([data[exp][0][j] + offset for j in range(len(data[exp][0]))], data[exp][3], 3, yerr = data[exp][4], capsize = 4, label = label, edgecolor = colors[label], hatch = patterns[label], fill = False)

    ax1.set_xlabel('Connections (per client; 56 clients)')
    ax1.set_ylabel('QPS (x1000)')
    ax1.set_ylim(ymin = 0, ymax = 150)
    ax2.set_ylabel('LLC misses per query')
    ax2.set_ylim(ymin = 0, ymax = 120)

    ax1.legend(loc = 'upper left')
    ax2.legend(loc = 'upper right')
    if os.getenv('SAVE_FIGURE') != 'true':
        plt.rcParams.update({'font.size': 16})
        plt.show()
    else:
        plt.savefig('../data/figs/memcached_conn_scan.t%d.png' % (threads,), dpi = 192)

plot(1)
