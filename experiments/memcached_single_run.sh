source "$EXP_ROOT/experiments/memcached.shared.sh"

pre_start() {
    memcached_pre_start
    EXPERIMENT_NAME_EXT="t$THREADS"
    # 5-second interval delta for perf stat, so that we can manually subtract startup phases
    PERF_STAT_DELTA=true
    CONNS=160

    while [ ! -z "$1" ]; do
        case "$1" in
            --perf-record)
                PERF_RECORD_TYPE="$2"
                EXPERIMENT_NAME_EXT="perf_$PERF_RECORD_TYPE.$EXPERIMENT_NAME_EXT"
                shift
                shift
                ;;
            --force-napi-locality)
                MEMCACHED_ARGS="$MEMCACHED_ARGS -N $THREADS" # We can't use NAPI_LOCALITY because it is handled in memcached_pre_start
                EXPERIMENT_NAME_EXT="force_napi_locality.$EXPERIMENT_NAME_EXT"
                shift
                ;;
            -c|--connections)
                CONNS="$2"
                EXPERIMENT_NAME_EXT="c$2.$EXPERIMENT_NAME_EXT"
                shift
                shift
                ;;
            *)
                echo "Unknown option"
                exit 1
                ;;
        esac
    done
}

run_exp() {
    memcached_init

    QPS=0

    [ ! -z "$PERF_RECORD_TYPE" ] && perf_record_start
    run_mutilate c$CONNS
    [ ! -z "$PERF_RECORD_TYPE" ] && perf_record_stop

    memcached_deinit
}

post_start() {
    memcached_deinit
}
