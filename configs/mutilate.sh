# The server IP (the current machine)
MEMSERVER_IP=192.168.199.1
# List of Mutilate clients
MEMCLIENTS=tilly03,tilly04,tilly05,tilly06,tilly07,tilly08
MEMCTRL=tilly02
# Command shorthands
MUTILATE="ssh $MEMCTRL taskset -c 0-7 mutilate -T8"
MUTILATE+=" -s $MEMSERVER_IP -d1 -K fb_key -V fb_value -i fb_ia -r1000000"
MUTCLIENTS=$(for m in $(echo $MEMCLIENTS|tr , ' '); do echo " -a $m";done)
