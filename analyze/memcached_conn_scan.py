import os
import util

def parse_memcached_output(s):
    lines = s.split('\n')
    qps = int(float(lines[6].split(' ')[3]))
    return qps

def parse_perf_stat(s, qps):
    lines = s.split('\n')
    load_misses = 0
    for line in lines:
        if line.startswith('#'):
            continue
        split = line.strip().split()
        if len(split) <= 1:
            continue
        if int(float(split[0])) != 25:
            continue
        if split[2] == 'LLC-load-misses':
            load_misses += int(split[1].replace(',', ''))
    return load_misses / qps / 5

def extract_conn_qps_exp(exp):
    ret = ([10, 20, 40, 60, 80, 100], [], [], [], [], [], [])

    for f in ret[0]:
        (qps_avg, qps_stddev) = util.extract_exp_avg_stddev(exp, 'c%d' % (f,), parse_memcached_output)
        ret[1].append(qps_avg)
        ret[2].append(qps_stddev)
 
        (misses_avg, misses_stddev) = \
            util.extract_exp_avg_stddev(exp, 'stat_c%d' % (f,), parse_perf_stat, qps_avg)
        ret[3].append(misses_avg)
        ret[4].append(misses_stddev)

    return ret

def collect_data(experiments):
    ret = dict()

    for exp in experiments:
        ret[exp] = extract_conn_qps_exp(exp)

    return ret
