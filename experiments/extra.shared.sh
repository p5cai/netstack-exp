perf_stat_start() {
    local opt_args=""
    if [ "$PERF_STAT_NO_CORES" != "true" ]; then
        opt_args="$opt_args -C $FIRST_CPU-$((FIRST_CPU + THREADS - 1))"
    fi
    if [ "$PERF_STAT_DELTA" == "true" ]; then
        opt_args="$opt_args -I 5000"
    fi
    $PERF stat $PERF_EXTRA_ARGS $opt_args -e LLC-load-misses:u,LLC-load-misses:k,LLC-loads:u,LLC-loads:k,LLC-store-misses:u,LLC-store-misses:k,LLC-stores:u,LLC-stores:k,task-clock,cycles:u,cycles:k,instructions:u,instructions:k -o "$DATA_OUT/stat_$1$EXP_AUX_SUFFIX.txt" &
    PERF_STAT_PID=$!
}

perf_stat_stop() {
    kill -INT "$PERF_STAT_PID" || true
}

perf_record_start() {
    local opt_args=""
    if [ "$PERF_STAT_NO_CORES" != "true" ]; then
        opt_args="$opt_args -C $FIRST_CPU-$((FIRST_CPU + THREADS - 1))"
    fi

    pushd "$DATA_OUT"
    $PERF record $PERF_EXTRA_ARGS $opt_args -e $PERF_RECORD_TYPE -F 1000 &
    PERF_RECORD_PID=$!
    popd
}

perf_record_stop() {
    kill -INT "$PERF_RECORD_PID" || true
    chmod 644 "$DATA_OUT/perf.data"
}

extra_data_start() {
    for m in $(echo "$1" | tr , ' '); do
        sudo -u $SSH_USER ssh $m "bash -s $NET_IF" < "$EXP_ROOT/tcp.sh" > /dev/null
    done
    "$EXP_ROOT/irq.sh" > /dev/null
}

extra_data_stop() {
    "$EXP_ROOT/irq.sh" > "$DATA_OUT/irq_$2$EXP_AUX_SUFFIX.txt"
    for m in $(echo "$1" | tr , ' '); do
        sudo -u $SSH_USER ssh $m "bash -s $NET_IF" < "$EXP_ROOT/tcp.sh" >> "$DATA_OUT/tcp_$2$EXP_AUX_SUFFIX.txt"
    done
}
