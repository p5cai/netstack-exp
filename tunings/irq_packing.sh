if [ "$THREADS" -ne 8 ]; then
    echo "IRQ packing can only be done with 8 threads"
    exit 1
fi

export THREAD_TAIL_OFFSET=2

# IRQ packing 2-2-6 scheme
ethtool -L $NET_IF rx 2 tx 8
$EXP_ROOT/irq.sh set $FIRST_CPU $((FIRST_CPU + 7))
$EXP_ROOT/irq.sh set rx $((FIRST_CPU + 6)) $((FIRST_CPU + 7))
export MEMCACHED_TARGET_QPS_PER_THREAD=130000

if [ "$FIRST_CPU" -gt 0 ]; then
    export MEMCACHED_TARGET_QPS_PER_THREAD=90000
fi
