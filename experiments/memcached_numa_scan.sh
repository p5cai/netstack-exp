source "$EXP_ROOT/experiments/memcached.shared.sh"

pre_start() {
    memcached_pre_start
    EXPERIMENT_NAME_EXT=""
    PERF_STAT_DELTA=true
}

run_exp() {
    CONNS=20
    QPS=0
    for THREADS in 2 4 8 16; do
        export THREADS
        export FIRST_CPU=$((8 - (THREADS / 2)))
        apply_tunings
        memcached_init
        run_mutilate t$THREADS
        memcached_deinit
    done
}

post_start() {
    memcached_deinit
}
