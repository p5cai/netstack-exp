import matplotlib.pyplot as plt
from memcached_conn_scan import collect_data
import os

def plot(threads):
    data = collect_data(threads, True)
    plt.cla()
    plt.title('Memcached Instructions per Request, c160, %d threads' % (threads,))

    plt.bar(range(len(data)), [data[k][5][4] for k in data], yerr = [data[k][6][4] for k in data], capsize=4)
    plt.xticks(range(len(data)), data.keys())

    plt.ylabel('Instructions per Request')

    plt.legend()
    if os.getenv('SAVE_FIGURE') != 'true':
        plt.rcParams.update({'font.size': 16})
        plt.show()
    else:
        plt.savefig('../data/figs/memcached_conn_scan.ipr.t%d.png' % (threads,), dpi = 192)

plot(1)
plot(8)
