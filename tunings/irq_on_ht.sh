if [ "$FIRST_CPU" -ne 0 ]; then
    echo "FIRST_CPU must be 0"
    exit 1
fi

$EXP_ROOT/irq.sh set 16 $((THREADS + 16 - 1)) 
$EXP_ROOT/irq.sh set rx 16 $((THREADS + 16 - 1))
