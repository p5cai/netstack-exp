import matplotlib.pyplot as plt
from memcached_numa_scan import collect_data
import os
from config import colors, patterns

experiments = [
    'memcached_numa_scan.5.15.79-peter', # vanilla
    'memcached_numa_scan.5.15.79-peter.kernel_polling', # kernel_polling
]

labels = [
    'Vanilla',
    'Kernel Polling',
]

def plot():
    data = collect_data(experiments)
    plt.cla()

    plt.xticks([1, 2, 3, 4], ["1 + 1", "2 + 2", "4 + 4", "8 + 8"])
    l = len(experiments)
    for i in range(l):
        offset = (0.4 * i + 0.2) - 0.4 * l / 2
        exp = experiments[i]
        plt.bar([1 + offset, 2 + offset, 3 + offset, 4 + offset], [data[exp][0][j] / 1000 for j in range(len(data[exp][0]))], 0.4, yerr = [data[exp][1][j] / 1000 for j in range(len(data[exp][1]))], capsize=4, label = labels[i], edgecolor = colors[labels[i]], hatch = patterns[labels[i]], fill = False)

    plt.ylabel('QPS (x1000)')
    plt.xlabel('Core Configuration')
    plt.ylim(0)

    plt.legend()
    if os.getenv('SAVE_FIGURE') != 'true':
        plt.rcParams.update({'font.size': 16})
        plt.show()
    else:
        plt.savefig('../data/figs/memcached_numa_scan_qps.png', dpi = 192)

plot()
