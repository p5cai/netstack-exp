source "$EXP_ROOT/experiments/nginx.shared.sh"

pre_start() {
    nginx_pre_start
    PERF_STAT_DELTA=true

    while [ ! -z "$1" ]; do
        case "$1" in
            --perf-record)
                PERF_RECORD_TYPE="$2"
                EXPERIMENT_NAME_EXT="perf_$PERF_RECORD_TYPE.$EXPERIMENT_NAME_EXT"
                shift
                shift
                ;;
            *)
                echo "Unknown option"
                exit 1
                ;;
        esac
    done
}

run_exp() {
    nginx_start
    nginx_warmup
    [ ! -z "$PERF_RECORD_TYPE" ] && perf_record_start
    nginx_bench 1000
    [ ! -z "$PERF_RECORD_TYPE" ] && perf_record_stop
    nginx_stop
}

post_start() {
    nginx_stop
}
