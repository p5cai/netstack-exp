IS_CUSTOM_KERNEL=false
CPUPOWER=cpupower
X86_ENERGY_PERF_POLICY=x86_energy_perf_policy
PERF=perf
FSTACK_SRC=/home/p5cai/workspace/f-stack
CALADAN_SRC=/home/p5cai/workspace/caladan

# Execute all SSH commands under this user
# so that we don't need ssh keys for root
SSH_USER=p5cai

# Default No. of threads
# Note that not all experiments honor this setting
THREADS=8
