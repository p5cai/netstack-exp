import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerTuple
from config import markers, colors
from memcached_qps_scan import extract_qps_latency
import os

experiments = [
  "memcached_qps_scan.5.15.79-peter.irq_packing.first2.t8.c20", # irq packing
  "memcached_qps_scan.5.15.79-peter.first4.t8.c20", # vanilla
  "memcached_qps_scan.5.15.79-peter.kernel_polling.first4.t8.c20", # kernel polling
]

experiments_uma = [
  "memcached_qps_scan.5.15.79-peter.irq_packing.t8.c20", # irq packing
  "memcached_qps_scan.5.15.79-peter.t8.c20", # vanilla
  "memcached_qps_scan.5.15.79-peter.kernel_polling.t8.c20", # kernel polling
]

names = [
  "IRQ Packing",
  "Vanilla",
  "Kernel Polling",
]

def show_plot():
    plt.cla()

    data = extract_qps_latency(experiments)
    data_uma = extract_qps_latency(experiments_uma)

    p = []
    pref = []

    for i in range(len(experiments)):
        k = experiments[i]
        display_name = names[i]
        p.append(plt.errorbar([data[k][0][j] / 1000 for j in range(len(data[k][0]))], data[k][1], yerr = data[k][2], capsize = 4, marker = markers[display_name], color = colors[display_name]))
        k = experiments_uma[i]
        pref.append(plt.errorbar([data_uma[k][0][j] / 1000 for j in range(len(data_uma[k][0]))], data_uma[k][1], linestyle = 'dotted', color = colors[display_name]))

    plt.xlabel('QPS (x1000)')
    plt.ylabel('Latency (99th percentile, μs)')
    plt.yscale('log')
    plt.xlim(xmin = 0)

    p.append((pref[0], pref[1], pref[2]))
    plt.legend(handles = p, labels = names + [ "reference" ], handler_map = {tuple: HandlerTuple(None)}, handlelength = 4, handleheight = 1.5)
    if os.getenv('SAVE_FIGURE') != 'true':
        plt.rcParams.update({'font.size': 16})
        plt.show()
    else:
        plt.savefig('../data/figs/memcached_qps_scan_numa.png', dpi = 192)

show_plot()
