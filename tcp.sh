#!/bin/bash

function error() {
	echo $*
	echo "usage $9 <device> [clear]"
	exit 1
}

if [ $# -lt 1 ] || [ ! ip link show dev $1 >/dev/null 2>&1 ]; then
	error "device $1 not found"
fi

cntfile=$HOME/.tcp.sh.count
[ -f $cntfile ] || touch $cntfile

[ "$2" = "clear" ] && {
	sentprev=0
	retransprev=0
	timeoutprev=0
} || {
	sentprev=$((grep -F sent: $cntfile || echo ":0")|cut -f2 -d:)
	retransprev=$((grep -F retrans: $cntfile || echo ":0")|cut -f2 -d:)
	timeoutprev=$((grep -F timeout: $cntfile || echo ":0")|cut -f2 -d:)
}

sentnow=$((netstat -s|grep -F "segments sent out" || echo 0)|awk '{print $1}')
retransnow=$((netstat -s|grep -F "segments retransmitted" || echo 0)|awk '{print $1}')
timeoutnow=$((netstat -s|grep -F "TCPTimeout" || echo 0 0 0)|awk '{print $2}')

echo "sent:$sentnow" > $cntfile
echo "retrans:$retransnow" >> $cntfile
echo "timeout:$timeoutnow" >> $cntfile

sent=$(expr $sentnow - $sentprev)
retrans=$(expr $retransnow - $retransprev)
timeout=$(expr $timeoutnow - $timeoutprev)

[ $sent -gt 0 ] && loss=$(printf "scale=6\n$timeout / $sent\n"|bc) || loss=0

[ $(expr $sent + $retrans + $timeout) -gt 0 ] && printf "sent %10d | retrans %10d | timeout %10d | loss rate %3.4f%%\n" $sent $retrans $timeout $loss

exit 0

