source "$EXP_ROOT/experiments/memcached.shared.sh"

QPS_STEPS_PER_THREAD="10000 40000 60000 65000 70000 75000 80000 85000 90000 95000 100000 105000 110000 115000 120000 125000 130000"

pre_start() {
    CONNS=20
    memcached_pre_start
}

run_exp() {
    memcached_init

    for QPS in $QPS_STEPS_PER_THREAD; do
        [[ "$QPS" -gt "$MEMCACHED_TARGET_QPS_PER_THREAD" ]] && break
        QPS=$((QPS * THREADS))
        export QPS
        run_mutilate $QPS
        sleep 15
    done    

    memcached_deinit
}

post_start() {
    memcached_deinit
}
