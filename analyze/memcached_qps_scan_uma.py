import matplotlib.pyplot as plt
from config import markers, colors
from memcached_qps_scan import extract_qps_latency
import os

experiments = [
  "memcached_qps_scan.5.15.79-peter.t8.c20", # vanilla
  "memcached_qps_scan.5.15.0-57-generic.manual_polling.t8.c20", # manual polling
  "memcached_qps_scan.5.15.79-peter.irq_packing.t8.c20", # irq packing
  "memcached_qps_scan.5.15.79-peter.kernel_polling.t8.c20", # kernel polling
]

names = [
  "Vanilla",
  "IRQ Suppression",
  "IRQ Packing",
  "Kernel Polling",
]

def show_plot():
    plt.cla()

    data = extract_qps_latency(experiments)

    for i in range(len(experiments)):
        k = experiments[i]
        display_name = names[i]
        plt.errorbar([data[k][0][j] / 1000 for j in range(len(data[k][0]))], data[k][1], yerr = data[k][2], capsize = 4, label = display_name, marker = markers[display_name], color = colors[display_name])

    plt.xlabel('QPS (x1000)')
    plt.ylabel('Latency (99th percentile, μs)')
    plt.yscale('log')
    plt.xlim(xmin = 0)

    plt.legend()
    if os.getenv('SAVE_FIGURE') != 'true':
        plt.rcParams.update({'font.size': 16})
        plt.show()
    else:
        plt.savefig('../data/figs/memcached_qps_scan_uma.png', dpi = 192)

show_plot()
